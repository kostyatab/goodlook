using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using GoodLook.Models;
using Microsoft.AspNet.Authorization;

namespace GoodLook.Controllers
{
    [Authorize(Roles = "SalonAdmin")]
    public class CategoryServicesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CategoryServicesController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: CategoryServices
        public async Task<IActionResult> Index()
        {
            var user = await GetUser();

            return View(await _context.CategoryServices.Include(x => x.Services).Where(x => x.Salon.Id == user.Salon.Id).ToListAsync());
        }

        // GET: CategoryServices/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            CategoryServices categoryServices = await _context.CategoryServices.FirstOrDefaultAsync(m => m.Id == id);
            if (categoryServices == null)
            {
                return HttpNotFound();
            }

            return View(categoryServices);
        }

        // GET: CategoryServices/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CategoryServices/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CategoryServices categoryServices)
        {
            if (ModelState.IsValid)
            {
                var user = await GetUser();

                if (user != null)
                {
                    categoryServices.Salon = user.Salon;

                    _context.CategoryServices.Add(categoryServices);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            return View(categoryServices);
        }

        // GET: CategoryServices/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var user = await GetUser();

            CategoryServices categoryServices = await _context.CategoryServices.FirstOrDefaultAsync(m => m.Id == id && m.Salon.Id == user.Salon.Id);
            if (categoryServices == null)
            {
                return HttpNotFound();
            }
            return View(categoryServices);
        }

        // POST: CategoryServices/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(CategoryServices categoryServices)
        {
            if (ModelState.IsValid)
            {
                var user = await GetUser();
                categoryServices.Salon = user.Salon;

                _context.Update(categoryServices);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(categoryServices);
        }

        // GET: CategoryServices/Delete/5
        [ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var user = await GetUser();

            CategoryServices categoryServices = await _context.CategoryServices.SingleAsync(m => m.Id == id && m.Salon.Id == user.Salon.Id);
            if (categoryServices == null)
            {
                return HttpNotFound();
            }

            return View(categoryServices);
        }

        // POST: CategoryServices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var user = await GetUser();

            CategoryServices categoryServices = await _context.CategoryServices.SingleAsync(m => m.Id == id && m.Salon.Id == user.Salon.Id);
            _context.CategoryServices.Remove(categoryServices);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private async Task<ApplicationUser> GetUser()
        {
            var user = await _context.Users
                .Include(x => x.Salon)
                .SingleAsync(x => x.Id == User.GetUserId());
            return user;
        }
    }
}
