using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using GoodLook.Models.Abstract;
using Microsoft.AspNet.Authorization;

namespace GoodLook.Controllers
{
    [Authorize(Roles = "SalonAdmin")]
    public class ClientsController : Controller
    {
        private readonly IAppRepository _repository;
        private readonly IClientManager _clientManager;

        public ClientsController(IAppRepository repository,
                                 IClientManager clientManager)
        {
            _repository = repository;
            _clientManager = clientManager;
        }

        // GET: Clients
        public async Task<IActionResult> Index()
        {
            return View(await _repository.Clients(User.GetUserId()));
        }

        [HttpPost]
        public async Task<IActionResult> Register(string phone, int salonId)
        {
            if (string.IsNullOrEmpty(phone))
            {
                return HttpBadRequest();
            }

            var code = await _clientManager.Register(phone, salonId);

            if (code == null)
            {
                return HttpBadRequest();
            }

            return Json(new {code});
        }

        [HttpPost]
        public async Task<IActionResult> ClienConfirm(string phone, string code, int salonId)
        {
            if (string.IsNullOrEmpty(code) || string.IsNullOrEmpty(phone))
            {
                return HttpBadRequest();
            }

            var token = await _clientManager.Confirm(phone, code, salonId);
            
            if (token == null)
            {
                return HttpBadRequest();
            }

            return Json(new {token});
        }
    }
}
