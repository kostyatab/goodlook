using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using GoodLook.Models;
using GoodLook.Models.Abstract;
using GoodLook.ViewModels;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;

namespace GoodLook.Controllers
{
    [Authorize(Roles = "SalonAdmin")]
    public class EmployeesController : Controller
    {
        private readonly IHostingEnvironment _env;
        private readonly IAppRepository _repository;

        public EmployeesController(IHostingEnvironment env,
            IAppRepository repository)
        {
            _env = env;
            _repository = repository;
        }

        // GET: Employees
        public async Task<IActionResult> Index()
        {
            return View(await _repository.Employees(User.GetUserId()));
        }

        // GET: Employees/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            return await GetEmployee(id);
        }

        // GET: Employees/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Employees/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Employees employees, IFormFile file)
        {
            if (!ModelState.IsValid)
            {
                return View(employees);
            }

            await _repository.SaveEmployee(employees, User.GetUserId());

            if (_env != null && file != null && file.Length > 0)
            {
                var uploadPath = Path.Combine(_env.WebRootPath, "upload", employees.Salon.Id.ToString(), "employees", employees.Id.ToString());
                Directory.CreateDirectory(uploadPath);

                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                await file.SaveAsAsync(Path.Combine(uploadPath, fileName));

                employees.Image = fileName;
                await _repository.SaveEmployee(employees, User.GetUserId());
            }

            return RedirectToAction("Index");
        }

        // GET: Employees/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            return await GetEmployee(id);
        }

        // POST: Employees/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Employees employees, IFormFile file)
        {
            if (!ModelState.IsValid)
            {
                return View(employees);
            }
            var dbEmployees = await _repository.GetEmployee(employees.Id, User.GetUserId());

            if (dbEmployees == null)
            {
                return HttpBadRequest();
            }
            dbEmployees.Name = employees.Name;

            await _repository.SaveEmployee(dbEmployees, User.GetUserId());

            if (_env != null && file != null && file.Length > 0)
            {
                var uploadPath = Path.Combine(_env.WebRootPath, "upload", dbEmployees.Salon.Id.ToString(), "news", dbEmployees.Id.ToString());
                Directory.CreateDirectory(uploadPath);

                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                await file.SaveAsAsync(Path.Combine(uploadPath, fileName));

                dbEmployees.Image = fileName;
                await _repository.SaveEmployee(dbEmployees, User.GetUserId());
            }

            return RedirectToAction("Index");
        }

        // GET: Employees/Delete/5
        [ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            return await GetEmployee(id);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employee = await _repository.GetEmployee(id, User.GetUserId());

            if (employee == null)
            {
                return HttpBadRequest();
            }

            await _repository.DeleteEmployee(employee, User.GetUserId());
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> AddService(int id)
        {
            var services = await _repository.Services(User.GetUserId());
            var employees = await _repository.GetEmployee(id, User.GetUserId());

            if (employees == null)
            {
                return HttpNotFound();
            }

            var serviceItemsList = services.GroupJoin(
                employees.ServiceEmployees,
                x => x.Id,
                y => y.Service.Id,
                (x, gj) => new { x, gj })
                .SelectMany(
                    @t => @t.gj.DefaultIfEmpty(),
                    (@t, subpet) => new EmployeesServiceItemViewModel() { Id = @t.x.Id, Display = @t.x.Name, IsChecked = (subpet != null)});

            var model = new EmployeesServiceViewModel()
            {
                IdEmployees = employees.Id,
                ServiceItems = serviceItemsList.ToList()
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddService(EmployeesServiceViewModel models)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest();
            }

            var employees = await _repository.GetEmployee(models.IdEmployees, User.GetUserId());

            if (employees == null)
            {
                return HttpBadRequest();
            }

            var serviceList = new List<ServiceEmployees>();
            foreach (var item in models.ServiceItems.Where(x=> x.IsChecked))
            {
                var service = await _repository.GetService(item.Id, User.GetUserId());

                if (service != null)
                {
                    serviceList.Add(new ServiceEmployees() {Employees = employees, Service = service});
                }
            }

            var removeService = await _repository.ServiceEmployees(models.IdEmployees);
            await _repository.DeleteServiceEmployees(removeService);

            await _repository.AddServiceEmployees(models.IdEmployees, serviceList, User.GetUserId());

            return RedirectToAction("Details", new {id = employees.Id});
        }

        private async Task<IActionResult> GetEmployee(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var employee = await _repository.GetEmployee(id.Value, User.GetUserId());

            if (employee == null)
            {
                return HttpNotFound();
            }

            return View(employee);
        }
    }
}

