using System;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using GoodLook.Models;
using GoodLook.Models.Abstract;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;

namespace GoodLook.Controllers
{
    [Authorize(Roles = "SalonAdmin")]
    public class NewsController : Controller
    {
        private readonly IHostingEnvironment _env;
        private readonly IAppRepository _repository;

        public NewsController(IHostingEnvironment env, IAppRepository repository)
        {
            _env = env;
            _repository = repository;
        }

        // GET: News
        public async Task<IActionResult> Index()
        {
            return View((await _repository.News(User.GetUserId())).OrderByDescending(x => x.DateCreate));
        }

        // GET: News/Details/5
        [Obsolete]
        public async Task<IActionResult> Details(int? id)
        {
            return await GetNews(id);
        }

        // GET: News/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: News/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(News news, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                await _repository.SaveNews(news, User.GetUserId());
                var user = await _repository.GetUserAsync(User.GetUserId());

                if (_env != null && file != null && file.Length > 0)
                {
                    var uploadPath = Path.Combine(_env.WebRootPath, "upload", user.Salon.Id.ToString(), "news", news.Id.ToString());
                    Directory.CreateDirectory(uploadPath);

                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    await file.SaveAsAsync(Path.Combine(uploadPath, fileName));

                    news.Image = fileName;
                    await _repository.SaveNews(news, User.GetUserId());
                }

                return RedirectToAction("Index");
            }
            return View(news);
        }

        // GET: News/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            return await GetNews(id);
        }

        // POST: News/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(News news, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                var user = await _repository.GetUserAsync(User.GetUserId());

                await _repository.SaveNews(news, User.GetUserId());

                if (_env != null && file != null && file.Length > 0)
                {
                    var uploadPath = Path.Combine(_env.WebRootPath, "upload", user.Salon.Id.ToString(), "news", news.Id.ToString());
                    Directory.CreateDirectory(uploadPath);

                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    await file.SaveAsAsync(Path.Combine(uploadPath, fileName));

                    news.Image = fileName;

                    await _repository.SaveNews(news, User.GetUserId());
                }

                return RedirectToAction("Index");
            }
            return View(news);
        }

        // GET: News/Delete/5
        [ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            return await GetNews(id);
        }

        // POST: News/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _repository.DeleteNews(id, User.GetUserId());
            return RedirectToAction("Index");
        }


        //Help Methods
        private async Task<IActionResult> GetNews(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var news = await _repository.GetNews(id.Value, User.GetUserId());

            if (news == null)
            {
                return HttpNotFound();
            }

            return View(news);
        }
    }
}
