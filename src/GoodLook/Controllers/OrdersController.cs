using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using GoodLook.Models;
using GoodLook.Models.Abstract;
using GoodLook.ViewModels;
using Microsoft.AspNet.Authorization;

namespace GoodLook.Controllers
{
    [Authorize(Roles = "SalonAdmin")]
    public class OrdersController : Controller
    {
        private readonly IAppRepository _repository;
        private readonly IOrdersManager _ordersManager;

        public OrdersController(IAppRepository repository, IOrdersManager ordersManager)
        {
            _repository = repository;
            _ordersManager = ordersManager;
        }

        // GET: Orders
        public async Task<IActionResult> Index()
        {
            return View(await _repository.Orders(User.GetUserId()));
        }

        // GET: Orders/Create
        public async Task<IActionResult> Create()
        {
            var model = new CreateOrderViewModel()
            {
                Services = await _repository.Services(User.GetUserId()),
                ScheduledDate = DateTime.Now,
                ScheduledTime = TimeSpan.Zero
            };
            return View(model);
        }

        // POST: Orders/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateOrderViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await _ordersManager.AddOrder(model.ClientPhone,
                model.ServiceId,
                model.ScheduledDate + model.ScheduledTime,
                User.GetUserId());

            if (result == null)
            {
                return View(model);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CancelOrder(int orderId)
        {
            var order = await _ordersManager.CancelOrder(orderId, User.GetUserId());

            if (order == null)
            {
                return HttpBadRequest();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ConfirmedOrder(int orderId)
        {
            var order = await _ordersManager.ConfirmedOrder(orderId, User.GetUserId());

            if (order == null)
            {
                return HttpBadRequest();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DoneOrder(int orderId)
        {
            var order = await _ordersManager.DoneOrder(orderId, User.GetUserId());

            if (order == null)
            {
                return HttpBadRequest();
            }

            return RedirectToAction("Index");
        }
    }
}
