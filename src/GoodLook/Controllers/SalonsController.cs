using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using GoodLook.Models;
using GoodLook.Models.Abstract;
using GoodLook.ViewModels;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Identity;

namespace GoodLook.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SalonsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAppRepository _repository;

        public SalonsController(ApplicationDbContext context, 
            UserManager<ApplicationUser> userManager,
            IAppRepository repository)
        {
            _context = context;
            _userManager = userManager;
            _repository = repository;
        }

        // GET: Salons
        public async Task<IActionResult> Index()
        {
            return View((await _repository.Salons()).ToList());
        }

        // GET: Salons/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            return await GetSalon(id);
        }

        // GET: Salons/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Salons/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Salon salon)
        {
            if (!ModelState.IsValid)
            {
                return View(salon);
            }

            await _repository.SaveSalon(salon);

            var settings = new SalonSettings()
            {
                Salon = salon
            };
            await _repository.CreateSalonSettings(settings);

            return RedirectToAction("Index");
        }

        // GET: Salons/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            return await GetSalon(id);
        }

        // POST: Salons/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Salon salon)
        {
            if (!ModelState.IsValid)
            {
                return View(salon);
            }
            await _repository.SaveSalon(salon);
            return RedirectToAction("Index");
        }

        // GET: Salons/Delete/5
        [ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            return await GetSalon(id);
        }

        // POST: Salons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var salon = (await _repository.Salons()).FirstOrDefault(x => x.Id == id);

            if (salon == null)
            {
                return HttpBadRequest();
            }

            await _repository.DeleteSalon(salon);
            return RedirectToAction("Index");
        }

        [ActionName("RegisterUser")]
        public IActionResult RegisterUser(int? salonId)
        {
            if (salonId != null)
            {
                var model = new RegisterUserViewModel() { SalonId = salonId.Value };
                return View(model);
            }

            return HttpNotFound();
        }

        [HttpPost, ActionName("RegisterUser")]
        public async Task<IActionResult> RegisterUser(RegisterUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var salon = await _context.Salons.SingleAsync(x => x.Id == model.SalonId);

                if (salon != null)
                {
                    var user = new ApplicationUser()
                    {
                        Salon = salon,
                        UserName = model.UserName
                    };

                    var result = await _userManager.CreateAsync(user, model.Password);
                    var resultAddRole = await _userManager.AddToRoleAsync(user, "SalonAdmin");

                    if (result.Succeeded && resultAddRole.Succeeded)
                    {
                        return RedirectToAction("Details", new {id = model.SalonId});
                    }
                }
            }

            return View(model);
        }

        [ActionName("DeleteUser")]
        public async Task<IActionResult> DeleteUser(string userId, int? salonId)
        {
            if (userId != null && salonId != null)
            {
                var user = await _context.Users
                    .Include(x => x.Salon)
                    .Where(x => (x.Id == userId && x.Salon.Id == salonId.Value))
                    .SingleAsync();

                if (user != null)
                {
                    return View(user);
                }
            }
            return HttpNotFound();
        }

        [HttpPost ,ActionName("DeleteUser")]
        public async Task<IActionResult> DeleteUser(ApplicationUser user)
        {
            var dbUser = await _context.Users
                .Include(x=> x.Salon)
                .SingleAsync(x => x.Id == user.Id);

            if (dbUser != null)
            {
                var result = await _userManager.DeleteAsync(dbUser);

                if (result.Succeeded)
                {
                    return RedirectToAction("Details", new { id = dbUser.Salon.Id});
                }
            }

            return View(user);
        }

        private async Task<IActionResult> GetSalon(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var news = (await _repository.Salons()).FirstOrDefault(x => x.Id == id);

            if (news == null)
            {
                return HttpNotFound();
            }

            return View(news);
        }
    }
}
