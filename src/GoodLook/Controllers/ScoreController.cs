﻿using System.Security.Claims;
using System.Threading.Tasks;
using GoodLook.Models.Abstract;
using GoodLook.ViewModels;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;

namespace GoodLook.Controllers
{
    [Authorize(Roles = "SalonAdmin")]
    public class ScoreController: Controller
    {
        private readonly IScoreManager _scoreManager;

        public ScoreController(IScoreManager scoreManager)
        {
            _scoreManager = scoreManager;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _scoreManager.GetSettings(User.GetUserId()));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Save(ScoreSettingViewModel model)
        {
            await _scoreManager.SaveSetting(model, User.GetUserId());
            return RedirectToAction("Index");
        }
    }
}
