using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using GoodLook.Models;
using GoodLook.ViewModels;
using Microsoft.AspNet.Authorization;

namespace GoodLook.Controllers
{
    [Authorize(Roles = "SalonAdmin")]
    public class ServicesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ServicesController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Services/Create
        public IActionResult Create(int categoryId)
        {
            var model = new ServiceViewModel()
            {
                CategoryId = categoryId,
                Service = new Service()
            };

            return View(model);
        }

        // POST: Services/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ServiceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await GetUser();

                var category = await 
                    _context.CategoryServices.FirstOrDefaultAsync(x => x.Id == model.CategoryId && x.Salon.Id == user.Salon.Id);

                if (category != null)
                {
                    model.Service.Category = category;
                    _context.Service.Add(model.Service);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index", "CategoryServices");
                }
            }
            return View(model);
        }

        // GET: Services/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var user = await GetUser();

            var service = await _context.Service.FirstOrDefaultAsync(m => m.Id == id && m.Category.Salon.Id == user.Salon.Id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // POST: Services/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Service service)
        {
            if (ModelState.IsValid)
            {
                var serviceDb = await _context.Service.Include(x => x.Category).FirstOrDefaultAsync(x => x.Id == service.Id);

                if (serviceDb != null)
                {
                    var user = await GetUser();
                    var category = await _context.CategoryServices.FirstOrDefaultAsync(x => x.Id == serviceDb.Category.Id && x.Salon.Id == user.Salon.Id);

                    if (category != null)
                    {
                        service.Category = category;
                        serviceDb.Name = service.Name;
                        serviceDb.Price = service.Price;

                        _context.Update(serviceDb);
                        await _context.SaveChangesAsync();
                        return RedirectToAction("Index", "CategoryServices");
                    }
                }
            }
            return View(service);
        }

        // GET: Services/Delete/5
        [ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Service service = await _context.Service.FirstOrDefaultAsync(m => m.Id == id);
            if (service == null)
            {
                return HttpNotFound();
            }

            return View(service);
        }

        // POST: Services/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var user = await GetUser();

            Service service = await _context.Service.FirstOrDefaultAsync(m => m.Id == id && m.Category.Salon.Id == user.Salon.Id);
            _context.Service.Remove(service);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "CategoryServices");
        }

        private async Task<ApplicationUser> GetUser()
        {
            var user = await _context.Users
                .Include(x => x.Salon)
                .SingleAsync(x => x.Id == User.GetUserId());
            return user;
        }
    }
}
