﻿using System.Security.Cryptography;
using System.Text;

namespace GoodLook.Helpers
{
    public static class CryptoHelper
    {
        public static string GetMd5(string input)
        {
            var text = Encoding.UTF8.GetBytes(input);
            var md5 = MD5.Create();
            var bytes = md5.ComputeHash(text);
            var retString = new StringBuilder();

            foreach (var b in bytes)
                retString.Append($"{b:x2}");

            return retString.ToString();
        }

        public static string GetSha256(string input)
        {
            var text = Encoding.UTF8.GetBytes(input);
            var sha256 = SHA256.Create();
            var bytes = sha256.ComputeHash(text);
            var retString = new StringBuilder();

            foreach (var b in bytes)
                retString.Append($"{b:x2}");

            return retString.ToString();
        }
    }
}
