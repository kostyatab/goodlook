﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoodLook.Models.Abstract
{
    public interface IAppRepository
    {
        //Orders
        Task<IEnumerable<Order>> Orders(string userId);
        Task<Order> SaveOrder(Order order, string userId);
        void DeleteOrder(int id, string userId);

        //Services
        Task<List<Service>> Services(string userId);
        Task<Service> GetService(int id, string userId);
        Task SaveService(IEnumerable<Service> services);

        //Clients
        Task<List<Client>> Clients(string userId);
        Task<List<Client>> Clients(int salonId);
        Task<Client> SaveClient(Client client, string userId);
        Task<Client> SaveClient(Client client);

        //News
        Task<List<News>> News(string userId);
        Task<News> SaveNews(News news, string userId);
        Task<News> GetNews(int newsId, string userId);
        Task<News> DeleteNews(int newsId, string userId);

        //User
        Task<ApplicationUser> GetUserAsync(string userId);

        //SalonSetting
        Task<SalonSettings> SalonSettings(string userId);
        Task SaveSalonSettings(SalonSettings settings, string userId);
        Task CreateSalonSettings(SalonSettings settings);

        //Salons
        Task<IEnumerable<Salon>> Salons();
        Task SaveSalon(Salon salon);
        Task DeleteSalon(Salon salon);

        //Score
        Task SaveScore(Score score, string userId);

        //Employees
        Task<List<Employees>> Employees(string userId);
        Task<Employees> GetEmployee(int id, string userId);
        Task SaveEmployee(Employees employees, string userId);
        Task DeleteEmployee(Employees employees, string userId);

        //ServiceEmployee
        Task<List<ServiceEmployees>> ServiceEmployees(int employeeId);
        Task DeleteServiceEmployees(IEnumerable<ServiceEmployees> serviceEmployeeses);
        Task AddServiceEmployees(int employeeId, IEnumerable<ServiceEmployees> serviceEmployeeses, string userId);
    }
}
