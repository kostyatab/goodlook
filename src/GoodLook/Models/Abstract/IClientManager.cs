﻿using System.Threading.Tasks;

namespace GoodLook.Models.Abstract
{
    public interface IClientManager
    {
        Task<string> Register(string phone, int salonId);
        Task<string> Confirm(string phone, string code, int salonId);
        Task<Client> AddClient(string phone, int salonId);
    }
}
