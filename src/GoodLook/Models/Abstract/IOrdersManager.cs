﻿using System;
using System.Threading.Tasks;

namespace GoodLook.Models.Abstract
{
    public interface IOrdersManager
    {
        Task<Order> AddOrder(string clientPhone, int serviceId, DateTime scheduledTime, string userId);
        Task<Order> CancelOrder(int orderId, string userId);
        Task<Order> ConfirmedOrder(int orderId, string userId);
        Task<Order> DoneOrder(int orderId, string userId);
    }
}
