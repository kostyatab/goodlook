﻿using System;
using System.Threading.Tasks;
using GoodLook.ViewModels;

namespace GoodLook.Models.Abstract
{
    public interface IScoreManager
    {
        Task SaveSetting(ScoreSettingViewModel model, string userId);
        Task<ScoreSettingViewModel> GetSettings(string userId);
        Task AddScore(int serviceId, Guid clientId, string userId);
    }
}
