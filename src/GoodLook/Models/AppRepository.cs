﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoodLook.Models.Abstract;
using Microsoft.Data.Entity;

namespace GoodLook.Models
{
    public class AppRepository : IAppRepository
    {
        private readonly ApplicationDbContext _context;

        public AppRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        //Orders
        public async Task<IEnumerable<Order>> Orders(string userId)
        {
            var user = await GetUserAsync(userId);
            return _context.Orders
                .Include(x => x.Client)
                .Include(x => x.Service)
                .Where(x => x.Salon.Id == user.Salon.Id);
        }

        public async Task<Order> SaveOrder(Order order, string userId)
        {
            Order orderReturn = null;
            if (order.Id == 0)
            {
                order.DateCreate = DateTime.Now;
                order.Salon = (await GetUserAsync(userId)).Salon;
                orderReturn = _context.Orders.Add(order).Entity;
            }
            else
            {
                orderReturn = _context.Orders.Update(order).Entity;
            }
            await _context.SaveChangesAsync();

            return orderReturn;
        }

        public void DeleteOrder(int id, string userId)
        {
            throw new NotImplementedException();
        }

        //Services
        public async Task<List<Service>> Services(string userId)
        {
            var user = await GetUserAsync(userId);

            return _context.Service
                .Include(x => x.ScoreService)
                .Include(x=> x.Category)
                .ThenInclude(x => x.Salon)
                .ToList()
                .Where(x => x.Category.Salon.Id == user.Salon.Id)
                .ToList();
        }

        public async Task<Service> GetService(int id, string userId)
        {
            var user = await GetUserAsync(userId);

            return _context.Service
                .Include(x => x.ScoreService)
                .Include(x => x.Category)
                .ThenInclude(x => x.Salon)
                .ToList()
                .FirstOrDefault(x => x.Category.Salon.Id == user.Salon.Id && x.Id == id);
        }

        public async Task SaveService(IEnumerable<Service> services)
        {
            _context.Service.UpdateRange(services);
            await _context.SaveChangesAsync();
        }

        //Clients
        public async Task<List<Client>> Clients(string userId)
        {
            var user = await GetUserAsync(userId);
            return await _context.Client.Where(x => x.Salon.Id == user.Salon.Id).ToListAsync();
        }

        public async Task<List<Client>> Clients(int salonId)
        {
            return await _context.Client.Where(x => x.Salon.Id == salonId).ToListAsync();
        }

        public async Task<Client> SaveClient(Client client, string userId)
        {
            Client returnClient = null;

            if (client.Id == Guid.Empty)
            {
                if (client.Salon == null)
                {
                    client.Salon = (await GetUserAsync(userId)).Salon;
                }
                returnClient = _context.Client.Add(client).Entity;
            }

            await _context.SaveChangesAsync();

            return returnClient;
        }

        public async Task<Client> SaveClient(Client client)
        {
            var returnClient = client.Id == Guid.Empty ? _context.Client.Add(client).Entity : _context.Client.Update(client).Entity;

            await _context.SaveChangesAsync();

            return returnClient;
        }

        //News
        public async Task<List<News>> News(string userId)
        {
            var user = await GetUserAsync(userId);

            return await _context.News.Where(x => x.Salon.Id == user.Salon.Id).ToListAsync();
        }

        public async Task<News> SaveNews(News news, string userId)
        {
            var user = await GetUserAsync(userId);
            news.Salon = user.Salon;

            News returnModel;
            if (news.Id == 0)
            {
                news.DateCreate = DateTime.Now;
                returnModel = _context.Add(news).Entity;
            }
            else
            {
                returnModel =_context.Update(news).Entity;
            }

            await _context.SaveChangesAsync();

            return returnModel;
        }

        public async Task<News> GetNews(int newsId, string userId)
        {
            var user = await GetUserAsync(userId);

            return await _context.News.FirstOrDefaultAsync(x =>x.Salon.Id == user.Salon.Id && x.Id == newsId);
        }

        public async Task<News> DeleteNews(int newsId, string userId)
        {
            var user = await GetUserAsync(userId);

            var news = await _context.News.FirstOrDefaultAsync(x => x.Salon.Id == user.Salon.Id && x.Id == newsId);
            _context.News.Remove(news);
            await _context.SaveChangesAsync();

            return news;
        }

        //User
        public async Task<ApplicationUser> GetUserAsync(string userId)
        {
            var user = await _context.Users
                .Include(x => x.Salon)
                .SingleAsync(x => x.Id == userId);
            return user;
        }

        //SalonSetting
        public async Task<SalonSettings> SalonSettings(string userId)
        {
            var user = await GetUserAsync(userId);

            return await _context.SalonSettings.FirstOrDefaultAsync(x => x.Salon.Id == user.Salon.Id);
        }

        public async Task SaveSalonSettings(SalonSettings settings, string userId)
        {
            settings.Salon = (await GetUserAsync(userId)).Salon;
            if (settings.Id == 0)
            {
                _context.SalonSettings.Add(settings);
            }
            else
            {
                _context.SalonSettings.Update(settings);
            }

            await _context.SaveChangesAsync();
        }

        public async Task CreateSalonSettings(SalonSettings settings)
        {
            _context.SalonSettings.Add(settings);

            await _context.SaveChangesAsync();
        }

        //Salons
        public async Task<IEnumerable<Salon>> Salons()
        {
            return await _context.Salons
                .Include(x => x.Clients)
                .Include(x => x.Users)
                .ToListAsync();
        }

        public async Task SaveSalon(Salon salon)
        {
            if (salon.Id == 0)
            {
                _context.Add(salon);
            }
            else
            {
                _context.Update(salon);
            }

            await _context.SaveChangesAsync();
        }

        public async Task DeleteSalon(Salon salon)
        {
            _context.Salons.Remove(salon);
            await _context.SaveChangesAsync();
        }

        public async Task SaveScore(Score score, string userId)
        {
            var user = await GetUserAsync(userId);

            score.Salon = user.Salon;
            score.User = user;

            _context.Score.Add(score);
            await _context.SaveChangesAsync();
        }

        //Employee
        public async Task<List<Employees>> Employees(string userId)
        {
            var user = await GetUserAsync(userId);

            return await _context.Employees
                .Where(x => x.Salon.Id == user.Salon.Id)
                .ToListAsync();
        }

        public async Task<Employees> GetEmployee(int id ,string userId)
        {
            var user = await GetUserAsync(userId);

            return await _context.Employees
                .Include(x => x.Salon)
                .Include(x => x.ServiceEmployees)
                .FirstOrDefaultAsync(x => x.Salon.Id == user.Salon.Id && x.Id == id);
        }

        public async Task SaveEmployee(Employees employees, string userId)
        {
            var user = await GetUserAsync(userId);
            if (employees.Id == 0)
            {
                employees.Salon = user.Salon;
                _context.Employees.Add(employees);
            }
            else
            {
                _context.Employees.Update(employees);
            }

            await _context.SaveChangesAsync();
        }

        public async Task DeleteEmployee(Employees employee, string userId)
        {
            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();
        }

        public async Task<List<ServiceEmployees>> ServiceEmployees(int employeeId)
        {
            return await _context.ServiceEmployeeses
                .Where(x => x.Id == employeeId)
                .ToListAsync();
        }

        public async Task DeleteServiceEmployees(IEnumerable<ServiceEmployees> serviceEmployeeses)
        {
            _context.ServiceEmployeeses.RemoveRange(serviceEmployeeses);
            await _context.SaveChangesAsync();
        }

        public async Task AddServiceEmployees(int employeeId, IEnumerable<ServiceEmployees> serviceEmployeeses, string userId)
        {
            var employee = await GetEmployee(employeeId, userId);

            if (employee == null)
            {
                return;
            }

            employee.ServiceEmployees.AddRange(serviceEmployeeses);
            await _context.SaveChangesAsync();
        }
    }
}
