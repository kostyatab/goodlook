﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using GoodLook.Models;

namespace GoodLook.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<Salon> Salons { get; set; }

        public DbSet<News> News { get; set; }

        public DbSet<Employees> Employees { get; set; }

        public DbSet<CategoryServices> CategoryServices { get; set; }

        public DbSet<Service> Service { get; set; }

        public DbSet<ServiceEmployees> ServiceEmployeeses { get; set; }

        public DbSet<Client> Client { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<SalonSettings> SalonSettings { get; set; }

        public DbSet<ScoreService> ScoreServices { get; set; }

        public DbSet<Score> Score { get; set; }
    }
}
