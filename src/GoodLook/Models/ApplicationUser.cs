﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace GoodLook.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public virtual Salon Salon { get; set; }
    }
}
