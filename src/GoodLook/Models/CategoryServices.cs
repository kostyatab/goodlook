﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GoodLook.Models
{
    public class CategoryServices
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public virtual List<Service> Services { get; set; }
        public virtual Salon Salon { get; set; }
    }
}
