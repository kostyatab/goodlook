﻿using System;

namespace GoodLook.Models
{
    public class Client
    {
        public Guid Id { get; set; }

        public string Phone { get; set; }

        public string Token { get; set; }

        public string Code { get; set; }

        public DateTime DateCreateCode { get; set; }

        public bool ClientConfirmed { get; set; }

        public virtual Salon Salon { get; set; }

        public virtual Score Score { get; set; }
    }
}
