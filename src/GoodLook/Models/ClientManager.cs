﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GoodLook.Helpers;
using GoodLook.Models.Abstract;

namespace GoodLook.Models
{
    public class ClientManager : IClientManager
    {
        private readonly IAppRepository _repository;

        public ClientManager(IAppRepository repository)
        {
            _repository = repository;
        }

        public async Task<string> Register(string phone, int salonId)
        {
            var salon = (await _repository.Salons()).FirstOrDefault(x => x.Id == salonId);

            if (salon == null)
            {
                return null;
            }

            var clientDb = salon.Clients.FirstOrDefault(x => x.Phone == phone);

            var code = GenerateCode();

            if (clientDb == null)
            {
                var client = new Client()
                {
                    Phone = phone,
                    DateCreateCode = DateTime.Now,
                    Salon = salon,
                    Code = code
                };

                await _repository.SaveClient(client);
            }
            else
            {
                clientDb.Code = code;

                await _repository.SaveClient(clientDb);
            }

            return code;
        }

        public async Task<string> Confirm(string phone, string code, int salonId)
        {
            var client = (await _repository.Clients(salonId))
                .FirstOrDefault(x => x.Phone == phone && x.Code == code);

            if (client == null)
            {
                return null;
            }

            client.Token = CryptoHelper.GetSha256(phone + code + DateTime.Now);
            client.ClientConfirmed = true;

            await _repository.SaveClient(client);

            return client.Token;
        }

        public async Task<Client> AddClient(string phone, int salonId)
        {
            var salon = (await _repository.Salons()).FirstOrDefault(x => x.Id == salonId);

            if (salon == null)
            {
                return null;
            }

            var client = new Client()
            {
                Phone = phone,
                Salon = salon
            };

            client = await _repository.SaveClient(client);

            return client;
        }

        private string GenerateCode()
        {
            var random = new Random();

            var returnVal = random.Next(0, 999999);

            return returnVal.ToString("D6");
        }
    }
}
