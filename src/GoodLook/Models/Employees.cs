﻿using System.Collections.Generic;

namespace GoodLook.Models
{
    public class Employees
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }

        public virtual Salon Salon { get; set; }
        public virtual List<ServiceEmployees> ServiceEmployees { get; set; }

    }
}
