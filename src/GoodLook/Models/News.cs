﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GoodLook.Models
{
    public class News
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Article { get; set; }

        public string Image { get; set; }
        public DateTime DateCreate { get; set; }

        public virtual Salon Salon { get; set; }
    }
}
