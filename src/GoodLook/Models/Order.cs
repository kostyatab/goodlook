﻿using System;

namespace GoodLook.Models
{
    public class Order
    {
        public int Id { get; set; }
        public Client Client { get; set; }
        public Service Service { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime ScheduledTime { get; set; }
        public OrderStatus Status { get; set; }
        public Salon Salon { get; set; }
    }

    public enum OrderStatus: byte
    {
        Create,
        Confirmed,
        Cancel,
        Done
    }
}
