﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GoodLook.Models.Abstract;

namespace GoodLook.Models
{
    public class OrdersManager : IOrdersManager
    {
        private readonly IAppRepository _repository;
        private readonly IClientManager _clientManager;
        private readonly IScoreManager _scoreManager;

        public OrdersManager(IAppRepository repository,
            IClientManager clientManager,
            IScoreManager scoreManager)
        {
            _repository = repository;
            _clientManager = clientManager;
            _scoreManager = scoreManager;
        }

        public async Task<Order> AddOrder(string clientPhone, int serviceId, DateTime scheduledTime, string userId)
        {
            var client = (await _repository.Clients(userId))
                .FirstOrDefault(x => x.Phone == clientPhone);

            var user = await _repository.GetUserAsync(userId);

            if (client == null)
            {
                client = await _clientManager.AddClient(clientPhone, user.Salon.Id);
            }

            var service = (await _repository.Services(userId)).FirstOrDefault(x => x.Id == serviceId);

            if (client == null || service == null)
            {
                return null;
            }

            var order = new Order()
            {
                Client = client,
                Salon = user.Salon,
                ScheduledTime = scheduledTime,
                Service = service,
                Status = OrderStatus.Confirmed
            };

            return await _repository.SaveOrder(order, userId);
        }

        public async Task<Order> CancelOrder(int orderId, string userId)
        {
            var order = (await _repository.Orders(userId)).FirstOrDefault(x => x.Id == orderId);

            if (order == null)
            {
                return null;
            }

            if (order.Status == OrderStatus.Done || order.Status == OrderStatus.Cancel)
            {
                return null;
            }

            order.Status = OrderStatus.Cancel;
            order = await _repository.SaveOrder(order, userId);

            return order;
        }

        public async Task<Order> ConfirmedOrder(int orderId, string userId)
        {
            var order = (await _repository.Orders(userId)).FirstOrDefault(x => x.Id == orderId);

            if (order?.Status != OrderStatus.Create)
            {
                return null;
            }

            order.Status = OrderStatus.Confirmed;
            order = await _repository.SaveOrder(order, userId);

            return order;
        }

        public async Task<Order> DoneOrder(int orderId, string userId)
        {
            var order = (await _repository.Orders(userId)).FirstOrDefault(x => x.Id == orderId);

            if (order?.Status != OrderStatus.Confirmed)
            {
                return null;
            }

            order.Status = OrderStatus.Done;
            order = await _repository.SaveOrder(order, userId);

            await _scoreManager.AddScore(order.Service.Id, order.Client.Id, userId);

            return order;
        }
    }
}