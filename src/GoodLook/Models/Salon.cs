﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GoodLook.Models
{
    public class Salon
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        public SalonSettings Settings { get; set; }
        public virtual List<News> News { get; set; }
        public virtual List<CategoryServices> CategoryServiceses { get; set; }
        public virtual List<Employees> Employees { get; set; }
        public virtual List<ApplicationUser> Users { get; set; }
        public virtual List<Client> Clients { get; set; }
        public virtual List<Order> Orders { get; set; }
    }
}
