﻿namespace GoodLook.Models
{
    public class SalonSettings
    {
        public int Id { get; set; }
        public int ScoreDuration { get; set; }

        public virtual Salon Salon { get; set; }
    }
}
