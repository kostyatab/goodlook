﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace GoodLook.Models
{
    public static class SampleData
    {
        private const string SalonAdminRole = "SalonAdmin";
        private const string AdminRole = "Admin";

        public static async void InitilizeData(IServiceProvider serviceProvider)
        {
            await CreateRoles(serviceProvider);
            await CreateAdminUser(serviceProvider);
        }

        private static async Task CreateAdminUser(IServiceProvider serviceProvider)
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json").Build();

            var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();
            var user = await userManager.FindByNameAsync(configuration["DefaultAdminData:UserName"]);

            if (user == null)
            {
                user = new ApplicationUser() { UserName = configuration["DefaultAdminData:UserName"] };
                await userManager.CreateAsync(user, configuration["DefaultAdminData:Password"]);
                await userManager.AddToRoleAsync(user, AdminRole);
            }
        }

        private static async Task CreateRoles(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetService<RoleManager<IdentityRole>>();

            if (!await roleManager.RoleExistsAsync(SalonAdminRole))
            {
                await roleManager.CreateAsync(new IdentityRole(SalonAdminRole));
            }
            if (!await roleManager.RoleExistsAsync(AdminRole))
            {
                await roleManager.CreateAsync(new IdentityRole(AdminRole));
            }
        }
    }
}
