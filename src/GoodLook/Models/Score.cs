﻿using System;

namespace GoodLook.Models
{
    public class Score
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime? DateEnd { get; set; }
        public string Descriprion { get; set; }

        public virtual Salon Salon { get; set; }
        public virtual Client Client { get; set; }
        public ApplicationUser User { get; set; }
    }
}
