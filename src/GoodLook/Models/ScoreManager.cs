﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoodLook.Models.Abstract;
using GoodLook.ViewModels;

namespace GoodLook.Models
{
    public class ScoreManager : IScoreManager
    {
        private readonly IAppRepository _repository;

        public ScoreManager(IAppRepository repository)
        {
            _repository = repository;
        }

        public async Task SaveSetting(ScoreSettingViewModel model, string userId)
        {
            var serviceList = new List<Service>();
            foreach (var service in model.Services)
            {
                var dbService = await _repository.GetService(service.Id, userId);
                if (dbService == null) continue;

                if (dbService.ScoreService == null)
                {
                    dbService.ScoreService = new ScoreService();
                }

                dbService.ScoreService.ScoreValue = service.ScoreService.ScoreValue;
                serviceList.Add(dbService);
            }

            await _repository.SaveService(serviceList);

            var salonSettings = await _repository.SalonSettings(userId);

            if (salonSettings == null)
            {
                salonSettings = new SalonSettings() {ScoreDuration = model.ScopeDuration};
            }
            else
            {
                salonSettings.ScoreDuration = model.ScopeDuration;
            }

            await _repository.SaveSalonSettings(salonSettings, userId);
        }

        public async Task<ScoreSettingViewModel> GetSettings(string userId)
        {
            var salonSetting = await _repository.SalonSettings(userId);
            var model = new ScoreSettingViewModel()
            {
                Services = await _repository.Services(userId),
            };
            if (salonSetting != null)
            {
                model.ScopeDuration = salonSetting.ScoreDuration;
            }

            return model;
        }

        public async Task AddScore(int serviceId, Guid clientId, string userId)
        {
            var salonSetting = await _repository.SalonSettings(userId);

            var client = (await _repository.Clients(userId)).FirstOrDefault(x => x.Id == clientId);
            var service = (await _repository.Services(userId)).FirstOrDefault(x => x.Id == serviceId);

            if (client == null || service == null)
            {
                return;
            }

            if (salonSetting == null)
            {
                var moder = await GetSettings(userId);
                await SaveSetting(moder, userId);

                salonSetting = await _repository.SalonSettings(userId);
            }

            var score = new Score
            {
                DateCreate = DateTime.Now,
                Client = client
            };

            if (salonSetting.ScoreDuration != 0)
            {
                score.DateEnd = DateTime.Now + new TimeSpan(salonSetting.ScoreDuration, 0, 0, 0);
            }

            score.Value = service.ScoreService.ScoreValue;

            score.Descriprion = $"Добавлены {score.Value} бонусов за {service.Name}.";

            await _repository.SaveScore(score, userId);
        }
    }
}
