﻿using Microsoft.Data.Entity.Metadata.Internal;

namespace GoodLook.Models
{
    public class ScoreService
    {
        public int Id { get; set; }
        public int ScoreValue { get; set; }

        public virtual Service Service { get; set; }
    }
}
