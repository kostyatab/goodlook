﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GoodLook.Models
{
    public class Service
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public decimal Price { get; set; }

        public virtual CategoryServices Category { get; set; }
        public virtual List<ServiceEmployees> ServiceEmployees { get; set; }
        public virtual List<Order> Orders { get; set; }
        public virtual ScoreService ScoreService { get; set; }
    }
}
