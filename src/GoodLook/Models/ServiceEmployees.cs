﻿namespace GoodLook.Models
{
    public class ServiceEmployees
    {
        public int Id { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual Service Service { get; set; }
    }
}
