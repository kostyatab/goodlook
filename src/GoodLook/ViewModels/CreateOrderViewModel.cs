﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GoodLook.Models;

namespace GoodLook.ViewModels
{
    public class CreateOrderViewModel
    {
        [Required]
        public string ClientPhone { get; set; }
        public List<Service> Services { get; set; }
        public int ServiceId { get; set; }
        [Required]
        public DateTime ScheduledDate { get; set; }
        [Required]
        public TimeSpan ScheduledTime { get; set; }
    }
}
