﻿using System.Collections.Generic;

namespace GoodLook.ViewModels
{
    public class EmployeesServiceViewModel
    {
        public int IdEmployees { get; set; }
        public List<EmployeesServiceItemViewModel> ServiceItems { get; set; }
    }

    public class EmployeesServiceItemViewModel
    {
        public int Id { get; set; }
        public string Display { get; set; }
        public bool IsChecked { get; set; }
    }
}
