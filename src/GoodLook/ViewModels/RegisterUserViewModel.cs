﻿using System.ComponentModel.DataAnnotations;

namespace GoodLook.ViewModels
{
    public class RegisterUserViewModel
    {
        [Required]
        [EmailAddress]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public int SalonId { get; set; }
    }
}
