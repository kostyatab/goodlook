﻿using System.Collections.Generic;
using GoodLook.Models;

namespace GoodLook.ViewModels
{
    public class ScoreSettingViewModel
    {
        public int ScopeDuration { get; set; }
        public List<Service> Services { get; set; }
    }
}
