﻿using GoodLook.Models;

namespace GoodLook.ViewModels
{
    public class ServiceViewModel
    {
        public int CategoryId { get; set; }
        public Service Service { get; set; }
    }
}
