///<reference path="jquery.d.ts"/>
$(document).ready(function () {
    var linkLogout = $('.js-logout');
    var logoutForm = $('#logoutForm');
    linkLogout.on('click', function (e) {
        e.preventDefault();
        logoutForm.submit();
    });
});
