﻿///<reference path="jquery.d.ts"/>

$(document).ready(() => {
  var linkLogout: JQuery = $('.js-logout');
  var logoutForm: JQuery = $('#logoutForm');

  linkLogout.on('click', (e) => {
    e.preventDefault();

    logoutForm.submit();
  });

});