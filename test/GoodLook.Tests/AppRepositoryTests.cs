﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using GoodLook.Models;
using GoodLook.Models.Abstract;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Http.Features.Authentication;
using Microsoft.AspNet.Http.Features.Authentication.Internal;
using Microsoft.AspNet.Http.Internal;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Http.Features;
using Microsoft.AspNet.Identity;
using Xunit;

namespace GoodLook.Tests
{
    public class AppRepositoryTests
    {
        private readonly IAppRepository _repository;
        private readonly ApplicationDbContext _context;
        private readonly ServiceCollection _services;

        public AppRepositoryTests()
        {
            _services = new ServiceCollection();

            _services.AddEntityFramework()
                      .AddInMemoryDatabase()
                      .AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase());

            _services.AddIdentity<ApplicationUser, IdentityRole>()
                        .AddEntityFrameworkStores<ApplicationDbContext>();

            var context = new DefaultHttpContext();
            context.Features.Set<IHttpAuthenticationFeature>(new HttpAuthenticationFeature());
            _services.AddSingleton<IHttpContextAccessor>(h => new HttpContextAccessor { HttpContext = context });
            
            _context = _services.BuildServiceProvider().GetRequiredService<ApplicationDbContext>();

            _repository = new AppRepository(_context);

            SampleDate().Wait(); 
        }

        private async Task SampleDate()
        {
            var userManager = _services.BuildServiceProvider().GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = _services.BuildServiceProvider().GetRequiredService<RoleManager<IdentityRole>>();

            var salonList = new List<Salon>()
            {
                new Salon() {Name = "TestSalon1", Description = "TestDescription1"},
                new Salon() {Name = "TestSalon2", Description = "TestDescription2"},
                new Salon() {Name = "TestSalon3", Description = "TestDescription3"}
            };

            _context.Salons.AddRange(salonList);
            _context.SaveChanges();

            var role = new IdentityRole("SalonAdmin");
            _context.Roles.Add(role);

            var userList = new List<ApplicationUser>()
            {
                new ApplicationUser() {UserName = "AdminSalon1", Id = "4e6c933d-5349-4263-9ceb-1095edcf49a0", Salon = salonList[0] },
                new ApplicationUser() {UserName = "AdminSalon11", Id = "74d59e2a-7ec6-4920-b4b7-8ec02172d5cc", Salon = salonList[0] },
                new ApplicationUser() {UserName = "AdminSalon2", Id = "d88824f1-0045-4190-a5c2-9120d4a1ea44", Salon = salonList[1] },
                new ApplicationUser() {UserName = "AdminSalon3", Id = "8bc14eec-0cb5-4edc-8335-6a6a0dc63a57", Salon = salonList[2] },
            };

            _context.Users.AddRange(userList);

            foreach (var user in userList)
            {
                _context.UserRoles.Add(new IdentityUserRole<string>() {RoleId = role.Id, UserId = user.Id});
            }

            await _context.SaveChangesAsync();

            var employeeList = new List<Employees>()
            {
                new Employees() { Name = "Employee1", Image = "1.png", Salon = salonList[0]},
                new Employees() { Name = "Employee11", Image = "2.png", Salon = salonList[0]},
                new Employees() { Name = "Employee13", Image = "3.png", Salon = salonList[0]},
                new Employees() { Name = "Employee2", Image = "6.png", Salon = salonList[1]},
                new Employees() { Name = "Employee2", Image = "4.png", Salon = salonList[1]},
                new Employees() { Name = "Employee3", Image = "5.png", Salon = salonList[2]},
            };

            _context.Employees.AddRange(employeeList);
            await _context.SaveChangesAsync();

            var salonSetting = new SalonSettings() {Salon = salonList[2], ScoreDuration = 16};
            _context.SalonSettings.Add(salonSetting);
            await _context.SaveChangesAsync();
        }

        [Fact]
        public async Task Salons_ReturnSalonsList()
        {
            //Arrange
            //Act
            var salons = await _repository.Salons();
            var collection = salons.ToList();

            //Assert
            Assert.NotEmpty(collection);
            Assert.Equal(collection.ToList().Count(), 3);
        }

        [Fact]
        public async Task SaveSalon_CreateAndSaveSalon()
        {
            //Arrange
            var salon = new Salon() {Name = "NewSalon", Description = "Description new salon"};

            //Act
            await _repository.SaveSalon(salon);

            //Assert
            var salonDb = await _context.Salons.FirstOrDefaultAsync(x => x.Name == "NewSalon");

            Assert.NotNull(salonDb);
            Assert.Equal(salon.Description, salonDb.Description);
            Assert.NotEqual(salon.Id, 0);
        }

        [Fact]
        public async Task SaveSalon_UpdateAndSaveSalon()
        {
            //Arrange
            var salon = await _context.Salons.FirstOrDefaultAsync(x => x.Name == "TestSalon1");

            //Act
            salon.Description = "TestDescription11";
            await _repository.SaveSalon(salon);

            //Assert
            var salonDb = await _context.Salons.FirstOrDefaultAsync(x => x.Name == "TestSalon1");

            Assert.Equal(salon.Description, salonDb.Description);
        }

        [Fact]
        public async Task DeleteSalon_DeleteSalonDb()
        {
            //Arrange
            var salon = await _context.Salons.FirstOrDefaultAsync(x => x.Name == "TestSalon2");
            
            //Act
            await _repository.DeleteSalon(salon);
            
            //Assert
            var salonDb = await _context.Salons.FirstOrDefaultAsync(x => x.Name == "TestSalon2");
            Assert.Null(salonDb);
        }

        [Fact]
        public async Task GetEmployee_employeesList()
        {
            //Arrange
            const string userId = "4e6c933d-5349-4263-9ceb-1095edcf49a0";

            //Act
            var list = (await _repository.Employees(userId)).ToList();

            //Assert
            Assert.NotNull(list);
            Assert.Equal(list.Count, 3);
        }

        [Fact]
        public async Task SaveEmployee_CreateNewEmployee()
        {
            //Arrange
            const string userId = "4e6c933d-5349-4263-9ceb-1095edcf49a0";
            var employee = new Employees() {Name = "Employee111", Image = "13.bmp"};

            //Act
            await _repository.SaveEmployee(employee, userId);

            //Assert
            var emplDb = await _context.Employees.FirstOrDefaultAsync(x => x.Name == "Employee111");
            Assert.NotNull(emplDb);
            Assert.Equal(emplDb.Name, emplDb.Name);
            Assert.Equal(emplDb.Image, emplDb.Image);
        }

        [Fact]
        public async Task SaveEmployee_UpdateEmployee()
        {
            //Arrange
            const string userId = "4e6c933d-5349-4263-9ceb-1095edcf49a0";
            var employee = await _context.Employees.FirstOrDefaultAsync(x => x.Name == "Employee1");

            //Act
            employee.Name = "Employee1-1";
            employee.Image = "123.ttt";
            await _repository.SaveEmployee(employee, userId);

            //Assert
            var empDb = await _context.Employees.FirstOrDefaultAsync(x => x.Id == employee.Id);
            Assert.NotNull(empDb);
            Assert.Equal(empDb.Name, employee.Name);
            Assert.Equal(empDb.Image, employee.Image);
        }

        [Fact]
        public async Task DeleteEmployee_DeleteEmployee()
        {
            //Arrange
            const string userId = "4e6c933d-5349-4263-9ceb-1095edcf49a0";
            var employee = await _context.Employees.FirstOrDefaultAsync(x => x.Name == "Employee11");

            //Act
            await _repository.DeleteEmployee(employee, userId);

            //Assert
            var employeeDb = await _context.Employees.FirstOrDefaultAsync(x => x.Name == "Employee11");
            Assert.Null(employeeDb);
        }

        [Fact]
        public async Task CreateSalonSettings_CreateSalonSetting()
        {
            //Arrange
            var salon = await _context.Salons.FirstOrDefaultAsync(x => x.Name == "TestSalon1");
            var settings = new SalonSettings() {Salon = salon, ScoreDuration = 12};

            //Act
            await _repository.CreateSalonSettings(settings);

            //Assert
            var salonDb = await _context.Salons.FirstOrDefaultAsync(x => x.Name == "TestSalon1");
            Assert.NotNull(salonDb.Settings);
            Assert.Equal(salonDb.Settings.ScoreDuration, 12);
        }

        [Fact]
        public async Task SaveSalonSettings_SaveSalonSettings()
        {
            //Arrange
            var salon = await _context.Salons.FirstOrDefaultAsync(x => x.Name == "TestSalon2");
            var settings = new SalonSettings() { Salon = salon, ScoreDuration = 13 };
            const string userId = "d88824f1-0045-4190-a5c2-9120d4a1ea44";

            //Act
            await _repository.SaveSalonSettings(settings, userId);

            //Assert
            var salonDb = await _context.Salons.FirstOrDefaultAsync(x => x.Name == "TestSalon2");
            Assert.NotNull(salonDb.Settings);
            Assert.Equal(salonDb.Settings.ScoreDuration, 13);
        }

        [Fact]
        public async Task SalonSettings_GetSalonSetting()
        {
            //Arrange
            const string userId = "8bc14eec-0cb5-4edc-8335-6a6a0dc63a57";

            //Act
            var salonSettingDb = await _repository.SalonSettings(userId);

            //Assert
            var salon = await _context.Salons.FirstOrDefaultAsync(x => x.Name == "TestSalon3");

            Assert.Equal(salonSettingDb.ScoreDuration, 16);
            Assert.Equal(salonSettingDb.ScoreDuration, salon.Settings.ScoreDuration);
        }
    }
}
